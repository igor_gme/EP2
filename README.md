		EP1 - Orientação a Objetos
		Professor: Renato Sampaio Coral
		Aluno: Igor Gabriel M. Evangelista
		Matricula: 15/0037074
		Turma: B

	Introdução

O objetivo deste projeto é uma interface gráfica com a linguagem java capaz de ler arquivos de imagens nos formatos PGM e PPM, além disso, extrair informações embutidas nestas imagens através da técnica de esteganografia.
O que o software faz

Abre imagens no formato PPM (coloridas) e PGM (preto e branco).
Descobre mensagem escondida em formato texto nas imagens PGM.
Descobre mensagem escondida em imagens coloridas por meio de aplicação de filtros.
Mostra a mensagem desccifrada (PGM).
Aplica os filtros Negativo, Sharpen e Smooth nas imagens PGM.
Como compilar

É necessário ter o programa NetBeans instalado.




