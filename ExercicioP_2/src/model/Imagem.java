package model;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author igor
 */
public abstract class Imagem {

    private String numeroMagico;
    private String caminho;
    private int largura;
    private int altura;
    private int valorMax;
    private int tamanhoImagem;
    private BufferedImage imagem;
    private FileInputStream arquivo;
    private byte[] pixel;
    private byte[] salvarImg;

    public Imagem() {
        this.altura = 0;
        this.largura = 0;
        this.numeroMagico = "";
        this.valorMax = 0;
        this.caminho = "";
    }

    public byte[] getSalvarImg() {
        return salvarImg;
    }

    public void setSalvarImg(byte[] salvarImg) {
        this.salvarImg = salvarImg;
    }

    
    
    public BufferedImage getImagem() {
        return imagem;
    }

    public void setImagem(BufferedImage imagem) {
        this.imagem = imagem;
    }

    public String getNumeroMagico() {
        return numeroMagico;
    }

    public void setNumeroMagico(String numeroMagico) {
        this.numeroMagico = numeroMagico;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getValorMax() {
        return valorMax;
    }

    public void setValorMax(int valorMax) {
        this.valorMax = valorMax;
    }

    public int getTamanhoImagem() {
        return tamanhoImagem;
    }

    public void setTamanhoImagem(int tamanhoImagem) {
        this.tamanhoImagem = tamanhoImagem;
    }

    public FileInputStream getArquivo() {
        return arquivo;
    }

    public void setArquivo(FileInputStream arquivo) {
        this.arquivo = arquivo;
    }

    public byte[] getPixel() {
        return pixel;
    }

    public void setPixel(byte[] pixel) {
        this.pixel = pixel;
    }

    public void abrirArquivo() throws FileNotFoundException {
        setArquivo(new FileInputStream(getCaminho()));
    }

    public void fecharArquivo() throws IOException {
        getArquivo().close();
    }

    public void adquirirNumeroMagico() throws IOException {
        int contador1 = 1, contador2 = 0;

        int caractere = getArquivo().read();

        while (caractere != -1) {
            if ((char) caractere == '#') {
                contador2--;
                contador1++;
            }
            if ((char) caractere == '\n') {
                contador2++;
                caractere = getArquivo().read();
                if ((char) caractere == '#') {
                    contador2--;
                    contador1++;
                }
            }
            if (contador2 == 0) {
                while (caractere != '\n') {
                    setNumeroMagico(getNumeroMagico() + (char) caractere);
                    caractere = getArquivo().read();
                    contador1++;
                }
                break;
            }
            contador1++;
            caractere = getArquivo().read();
        }
        setTamanhoImagem(contador1);
    }
    
    public static int verificarNumberMagic (String caminho) throws FileNotFoundException, IOException{
        int i = 0;
        FileInputStream arq = new FileInputStream(caminho);
        String numberMagic = "";
        String tipo = caminho.substring(caminho.length()-3, caminho.length());
        int caractere = arq.read();
        
        while(caractere != -1){
            if((char) caractere == '#'){
                i--;
            }
            if((char) caractere == '\n'){
                i++;
                caractere = arq.read();
                if((char) caractere == '#'){
                    i--;
                }
            }
            if(i == 0){
                while(caractere != '\n'){
                    numberMagic += (char) caractere;
                    caractere = arq.read();
                }
                break;
            }
            caractere = arq.read();
        }
        arq.close();
        if(numberMagic.equals("P5") && (tipo.equals("pgm"))){
            return 1;
        }
        else if(numberMagic.equals("P6") && (tipo.equals("ppm"))){
            return 2;
        }
        else{
            throw new IllegalArgumentException("Arquivo não Suportado ou corrompido.");
        }
    }
    
    public void adquirirLargura() throws IOException {

        String Largura = "";
        int contador1 = 1, contador2 = 0;
        int caracter = getArquivo().read();

        while (caracter != -1) {
            if ((char) caracter == '#') {
                contador2--;
                contador1++;

            }
            if ((char) caracter == '\n') {
                contador2++;
                caracter = getArquivo().read();
                if ((char) caracter == '#') {
                    contador2--;
                    contador1++;
                }
            }
            if (contador2 == 0) {
                while (caracter != '\n') {
                    Largura += (char) caracter;
                    caracter = getArquivo().read();
                    contador1++;
                }
                setLargura(Integer.parseInt(Largura));
                break;
            }
            contador1++;
            caracter = getArquivo().read();
        }
        setTamanhoImagem(getTamanhoImagem() + contador1);
    }

    public void adquirirAltura() throws IOException {
        String Altura = "";
        int contador1 = 1, contador2 = 0;
        int caractere = getArquivo().read();

        while (caractere != -1) {
            if ((char) caractere == '#') {
                contador2--;

            }
            if ((char) caractere == '\n') {
                contador2++;
                caractere = getArquivo().read();
                contador1++;
                if ((char) caractere == '#') {
                    contador2--;
                    contador1++;
                }
            }
            if (contador2 == 0) {
                while (caractere != ' ') {
                    Altura += (char) caractere;
                    contador1++;
                    caractere = getArquivo().read();
                }
                setAltura(Integer.parseInt(Altura));
                break;
            }
            contador1++;
            caractere = getArquivo().read();
        }
        setTamanhoImagem(getTamanhoImagem() + contador1);
    }

    public void adquirirValorMaximo() throws IOException {
        String valorMax = "";
        int contador1 = 1, contador2 = 0;
        int caractere = getArquivo().read();

        while (caractere != -1) {
            if ((char) caractere == '#') {
                contador2--;

            }
            if ((char) caractere == '\n') {
                contador2++;
                caractere = getArquivo().read();
                contador1++;
                if ((char) caractere == '#') {
                    contador2--;
                    contador1++;
                }
            }
            if (contador2 == 0) {
                while (caractere != '\n') {
                    valorMax += (char) caractere;
                    contador1++;
                    caractere = getArquivo().read();
                }
                setValorMax(Integer.parseInt(valorMax));
                break;
            }
            contador1++;
            caractere = getArquivo().read();
        }
        setTamanhoImagem(getTamanhoImagem() + contador1);
    }

    public abstract BufferedImage adquirirPixel() throws IOException;

    public abstract BufferedImage filtroNegativo() throws FileNotFoundException, IOException;

    public void pegarCabecalho() throws IOException {
        adquirirNumeroMagico();
        adquirirAltura();
        adquirirLargura();
        adquirirValorMaximo();
    }

    public Imagem(String caminho) {
        this.altura = 0;
        this.largura = 0;
        this.numeroMagico = "";
        this.valorMax = 0;
        this.caminho = caminho;
    }
}
