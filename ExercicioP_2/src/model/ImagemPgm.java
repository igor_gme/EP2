package model;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author igor
 */
public class ImagemPgm extends Imagem {

    private int posicaoMensagem;

    public ImagemPgm() {
        super();
        this.posicaoMensagem = 0;
    }

    public int getPosicaoMensagem() {
        return posicaoMensagem;
    }

    public void setPosicaoMensagem(int posicaoMensagem) {
        this.posicaoMensagem = posicaoMensagem;
    }

    public ImagemPgm(String caminho) {
        super(caminho);
        this.posicaoMensagem = 0;
    }

    public void pegarPosicaoMensagem() throws IOException {
        abrirArquivo();
        int caractere = getArquivo().read();
        int contador = 0;
        String posicaoInicial = "";
        while (caractere != -1) {
            if ((char) caractere == '#') {
                caractere = getArquivo().read();//
                caractere = getArquivo().read();
                while ((char) caractere != ' ' && (char) caractere != '\n') {
                    posicaoInicial += (char) caractere;
                    caractere = getArquivo().read();
                }
                setPosicaoMensagem(Integer.parseInt(posicaoInicial));
                break;
            }
            contador++;
            caractere = getArquivo().read();
        }
        fecharArquivo();
    }

    public String decifrarMensagem() {
        String segredo = "";
        int contador2 = 0;
        int pixel = 0;
        int caractere = 0;

        for (int contador1 = getPosicaoMensagem() + 1; contador2 < 8; contador1++) {
            caractere = pixel | (caractere << 1);
            pixel = ((int) getPixel()[contador1]) & 0x001;
            if (contador2 == 7) {
                if ((char) caractere != '#') {
                    segredo += ((char) caractere);
                    //System.out.print((char) caractere);
                }
                if (caractere != '#' || contador1 < getPosicaoMensagem() + 16) {
                    contador2 = -1;
                    caractere = 0;
                }
            }
            contador2++;
        }
        return segredo;
    }
    
    

public void salvarImagem(String endereco) throws FileNotFoundException, IOException{
        File file = new File(endereco + ".pgm");
        FileOutputStream temp;
        
            temp = new FileOutputStream(file);
            if (!file.exists()) {
                file.createNewFile();
            }
            temp.write(getNumeroMagico().getBytes());
            temp.write('\n');
            String comentario = "# "+getPosicaoMensagem();
            temp.write(comentario.getBytes());
            temp.write('\n');
            temp.write(Integer.toString(getAltura()).getBytes());
            temp.write(' ');
            temp.write(Integer.toString(getLargura()).getBytes());
            temp.write('\n');
            temp.write(Integer.toString(getValorMax()).getBytes());
            temp.write('\n');
            temp.write(getSalvarImg());
            temp.close();
      

    }


    @Override
    public BufferedImage adquirirPixel() throws IOException {
        BufferedImage img_pgm = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixels = ((DataBufferByte) img_pgm.getRaster().getDataBuffer()).getData();
        int i = 0, j=0;
        int caractere = getArquivo().read();
        while (caractere != -1) {
            if((char) caractere == '#'){
                j--;
            }
            if((char)caractere == '\n'){
                j++;
                caractere = getArquivo().read();
                if((char)caractere =='#'){
                    j--;
                }
            }
            if(j == 0){
                while(caractere != -1){
                    pixels[i] = (byte) caractere;
                    i++;
                    caractere = getArquivo().read();
                }
                break;
            }
            caractere = getArquivo().read();
        }
        setPixel(((DataBufferByte) img_pgm.getRaster().getDataBuffer()).getData());
        setPixel(pixels);
        return img_pgm;
    }

    @Override
    public BufferedImage filtroNegativo() throws FileNotFoundException, IOException {
        BufferedImage img_pgm_neg = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixels = ((DataBufferByte) img_pgm_neg.getRaster().getDataBuffer()).getData();
        int contador = 0;

        for (contador = 0; contador < getPixel().length; contador++) {
            pixels[contador] = (byte) (getValorMax() - getPixel()[contador]);
        }
        setSalvarImg(pixels);
        return img_pgm_neg;
    }
    
    public BufferedImage filtroSharpen() throws FileNotFoundException, IOException {
		int filtro[] = { 0, -1, 0, -1, 5, -1, 0, -1, 0 };
		int value;
		BufferedImage img_pgm_sharp = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_BYTE_GRAY);
		byte[] pixels3 = ((DataBufferByte) img_pgm_sharp.getRaster().getDataBuffer()).getData();
		char[] pixels = new char[getAltura() * getLargura()];
		abrirArquivo();
		setNumeroMagico("");
		pegarCabecalho();
		int contador = 0;
		int caractere = getArquivo().read();
		while (caractere != -1) {
                    if((char) caractere == '#'){
                        contador--;
                    }
                    if((char) caractere == '\n'){
                        contador++;
                        caractere = getArquivo().read();
                        if((char) caractere == '#'){
                            contador--;
                        }
                    }
                    if(contador == 0){
                        while(caractere != -1){
                            pixels[contador] = (char) caractere;
                            contador++;
                            caractere = getArquivo().read();
                        }
                        break;
                    }
                    caractere = getArquivo().read();
		}
		fecharArquivo();

		int i, j;   

		for (i = 0; i < 3; i++) {
			for (j = 0; j < getLargura(); j++) {
				pixels3[i + j * getLargura()] = (byte) getPixel()[i + j * getLargura()];
			}
		}

		for (i = 0; i < 3; i++) {
			for (j = 0; j < getAltura(); j++) {
				pixels3[i + j * getLargura()] = (byte) getPixel()[i + j * getLargura()];
			}
		}

		int x = 0, y = 0;

		for (i = 3 / 2; i < getLargura() - 3 / 2; i++) {
			for (j = 3 / 2; j < getAltura() - 3 / 2; j++) {
				value = 0;
				for (x = -3 / 2; x <= 3 / 2; x++) {
					for (y = -3 / 2; y <= 3 / 2; y++) {
						value += filtro[(x + 1) + 3 * (y + 1)] * (pixels[(i + x) + (y + j) * getLargura()]);
					}
				}

				value /= 1;
				value = value < 0 ? 0 : value;
				value = value > getValorMax() ? getValorMax() : value;
				pixels3[i + j * getLargura()] = (byte) value;
			}
		}
                setSalvarImg(pixels3);
		return img_pgm_sharp;
	}
        
        public BufferedImage filtroSmooth() throws FileNotFoundException, IOException {
		int filtro[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		int value;
		BufferedImage img_pgm_smooth = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_BYTE_GRAY);
		byte[] pixels3 = ((DataBufferByte) img_pgm_smooth.getRaster().getDataBuffer()).getData();
		char[] pixels = new char[getAltura() * getLargura()];
		abrirArquivo();
		setNumeroMagico("");
		pegarCabecalho();
		int contador = 0;
		int caractere = getArquivo().read();
		while (caractere != -1) {
                    if((char) caractere == '#'){
                        contador--;
                    }
                    if((char) caractere == '\n'){
                        contador++;
                        caractere = getArquivo().read();
                        if((char) caractere == '#'){
                            contador--;
                        }
                    }
                    if(contador == 0){
                        while(caractere != -1){
                            pixels[contador] = (char) caractere;
                            contador++;
                            caractere = getArquivo().read();
                        }
                        break;
                    }
                    caractere = getArquivo().read();
		}
		fecharArquivo();

		int i=0, j=0;

		for (i = 0; i < 3; i++) {
			for (j = 0; j < getLargura(); j++) {
				pixels3[i + j * getLargura()] = (byte) getPixel()[i + j * getLargura()];
			}
		}

		for (i = 0; i < 3; i++) {
			for (j = 0; j < getAltura(); j++) {
				pixels3[i + j * getLargura()] = (byte) getPixel()[i + j * getLargura()];
			}
		}

		int x = 0, y = 0;

		for (i = 3 / 2; i < getLargura() - 3 / 2; i++) {
			for (j = 3 / 2; j < getAltura() - 3 / 2; j++) {
				value = 0;
				for (x = -3 / 2; x <= 3 / 2; x++) {
					for (y = -3 / 2; y <= 3 / 2; y++) {
						value += filtro[(x + 1) + 3 * (y + 1)] * (pixels[(i + x) + (y + j) * getLargura()]);
					}
				}

				value /= 9;
				value = value < 0 ? 0 : value;
				value = value > getValorMax() ? getValorMax() : value;
				pixels3[i + j * getLargura()] = (byte) value;
			}
		}
                setSalvarImg(pixels3);
		return img_pgm_smooth;
	}
}
