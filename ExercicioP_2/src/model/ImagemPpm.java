package model;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author igor
 */
public class ImagemPpm extends Imagem {

    public ImagemPpm() {
        super();
    }
    public ImagemPpm(String caminho){
        super(caminho);
    }

    @Override
    public BufferedImage adquirirPixel() throws IOException {
        BufferedImage img_ppm = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixels = ((DataBufferByte) img_ppm.getRaster().getDataBuffer()).getData();
        int i = 0, j=0;
        int caractere = getArquivo().read();
        
        while(caractere != -1){
            if((char) caractere == '#'){
                j--;
            }
            if((char) caractere == '\n'){
                j++;
                caractere = getArquivo().read();
                if((char) caractere == '#'){
                    j--;
                }
            }
            if(j == 0){
                while (caractere != -1) {
                    if (i == getAltura() * getLargura() * 3) {
                    } else {
                        pixels[i + 2] = (byte) caractere;
                        i++;
                        caractere = getArquivo().read();
                        pixels[i] = (byte) caractere;
                        i++;
                        caractere = getArquivo().read();
                        pixels[i - 2] = (byte) caractere;
                        i++;
                    }
                    caractere = getArquivo().read();
                 }
                break;
            }
            caractere = getArquivo().read();
        }

        setPixel(((DataBufferByte) img_ppm.getRaster().getDataBuffer()).getData());
        setPixel(pixels);
        setSalvarImg(pixels);
        return img_ppm;
    }

    public BufferedImage filtroVermelho() throws FileNotFoundException, IOException {
        BufferedImage img_ppm_R = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixels = ((DataBufferByte) img_ppm_R.getRaster().getDataBuffer()).getData();
        int i = 0, j = 0;
        for (i = 0; i < getPixel().length; i++) {
            if (j == 2) {
                pixels[i] = (getPixel()[i]);
                j = 0;
            } else {
                pixels[i] = ((byte) 0);
                j++;
            }
        }
        setSalvarImg(pixels);
        return img_ppm_R;
    }

    public BufferedImage filtroVerde() throws FileNotFoundException, IOException {
        BufferedImage img_ppm_G = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixels = ((DataBufferByte) img_ppm_G.getRaster().getDataBuffer()).getData();
        int i = 0, j = 0;
        for (i = 0; i < getPixel().length; i++) {
            if (j == 1) {
                pixels[i] = (getPixel()[i]);
                j++;
            } else {
                pixels[i] = ((byte) 0);
                j++;
                if (j == 3) {
                    j = 0;
                }
            }
        }
        setSalvarImg(pixels);
        return img_ppm_G;
    }

    public BufferedImage filtroAzul() throws FileNotFoundException, IOException {
        BufferedImage img_ppm_B = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixels = ((DataBufferByte) img_ppm_B.getRaster().getDataBuffer()).getData();
        int i = 0, j = 0;
        for (i = 0; i < getPixel().length; i++) {
            if (j == 0) {
                pixels[i] = (getPixel()[i]);
                j++;
            } else {
                pixels[i] = ((byte) 0);
                j++;
                if (j == 3) {
                    j = 0;
                }
            }
        }
        setSalvarImg(pixels);
        return img_ppm_B;
    }

public void salvarImagem(String endereco) throws FileNotFoundException, IOException{
        File file = new File(endereco + ".ppm");
        FileOutputStream temp;
        
            temp = new FileOutputStream(file);
            if (!file.exists()) {
                file.createNewFile();
            }
            temp.write(getNumeroMagico().getBytes());
            temp.write('\n');
            temp.write(Integer.toString(getAltura()).getBytes());
            temp.write(' ');
            temp.write(Integer.toString(getLargura()).getBytes());
            temp.write('\n');
            temp.write(Integer.toString(getValorMax()).getBytes());
            temp.write('\n');
            for(int contador = 2; contador<getSalvarImg().length;contador+=3){
                temp.write(getSalvarImg()[contador]);
                temp.write(getSalvarImg()[contador-1]);
                temp.write(getSalvarImg()[contador-2]);
            }
            temp.close();
      

    }

    @Override
    public BufferedImage filtroNegativo() throws FileNotFoundException, IOException {
        BufferedImage img_ppm_neg = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixels = ((DataBufferByte) img_ppm_neg.getRaster().getDataBuffer()).getData();
        for (int contador = 0; contador < getPixel().length; contador++) {
            pixels[contador] = (byte) (getValorMax() - getPixel()[contador]);
        }
        setSalvarImg(pixels);
        return img_ppm_neg;
    }

}
